import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.junit.Test;

import DAO.DaoAuthentification;
import DAO.DaoClient;
import Model.Authentification;
import Model.Client;

public class DaoClientTest {

	@Test
	public void nombreClientTest() {
		// On regarde le nombre de client
		DaoClient daoCl = new DaoClient();
		int resultAvant = daoCl.nombreClient();
		System.out.println(resultAvant);
		// On ajoute un nouveau client dans la BDD
		DaoAuthentification daCreat = new DaoAuthentification();

		boolean nouveau = true;
		String login = "cc";
		String mdp = "nouveau";

		String loginExist = null;
		loginExist = daCreat.selectBylogin(login);

		// On fait une boucle tant qu'un nouveau login existe
		while (nouveau) {
			// On verifie si le login existe deja, car etant une cle primaire,
			// il faut toujours un nouveau login
			System.out.println("dans le while");
			if (!loginExist.equals(login))
				nouveau = false;

			login = login + "m";

		}
		// On cr�e un nouveau login
		System.out.println(login);
		Authentification authCreat = new Authentification();
		authCreat.setDroit_acces(false);
		authCreat.setLogin(login);
		authCreat.setMdp(mdp);

		Client client = new Client();
		// client.setId(32);
		client.setNom("bbb");
		client.setPrenom("bbb");
		client.setAdresse("bbb");

		daCreat.insertClient(authCreat, client);

		// On r�cup�re le nouveau nombre de client et on compare les deux
		int resultApres = daoCl.nombreClient();

		daCreat.Delete(login);

		if (resultApres - resultAvant != 1)
			fail("nombre client");
	}

	 @Test
	 public void updateTest() throws ClassNotFoundException, SQLException {
	
	 String login = "updtest1";
	 String mdp = "updtest";
	 DaoAuthentification daCreat = new DaoAuthentification();
	 DaoClient daoCl = new DaoClient();
	 boolean nouveau = true;
		

		String loginExist = null;
		loginExist = daCreat.selectBylogin(login);

		// On fait une boucle tant qu'un nouveau login existe
		while (nouveau) {
			// On verifie si le login existe deja, car etant une cle primaire,
			// il faut toujours un nouveau login
			System.out.println("dans le while");
			if (!loginExist.equals(login))
				nouveau = false;

			login = login + "m";

		}
	
	 // On cr�e un client
	String prenom ="ancienprenom";
	String nom ="anciennom";
		
	 Client client1 = new Client();
	 client1.setTelephone("0102030405");
	 client1.setNom(nom);
	 client1.setPrenom(prenom);
	 client1.setAdresse("ancienadresse");
	
	 // J'ajoute ce client dans la BDD

	 Authentification authCreat = new Authentification();
	 authCreat.setLogin(login);
	 authCreat.setMdp(mdp);
	 authCreat.setDroit_acces(false);
	
	
	 daCreat.insertClient(authCreat, client1);
	
	 Authentification auth = null;
	 auth = daCreat.selectByIdMdp(login, mdp);
	// int ClientId =  daoCl.selectByNomPrenom(nom, prenom).getId();
	int clientId = auth.getClient().getId();
	
	 // On recupere la version du client
	 int versionAvant = auth.getClient().getVersion();
	
	 // On cree un nouveau client
	
	 Client client2 = new Client();
	 client2.setId(clientId);
	 client2.setTelephone("09999999");
	 client2.setNom("nouveaunom");
	 client2.setPrenom("nouveauprenom");
	 client2.setAdresse("nouvelleadresse");
	
	 // On modifie les informations du client avec la methode Updtate
	
	 daoCl.update(client2);
	 Authentification auth2 = null;
	
	 auth2 = daCreat.selectByIdMdp(login, mdp);
	 daCreat.Delete(login);
	 // On v�rifie si les informations sont diff�rentes
	 if(auth2.getClient().getAdresse().equals(client1.getAdresse()))
	 fail("pas de changement d'adresse");
	 if(auth2.getClient().getNom().equals(client1.getNom()))
	 fail("pas de changement de nom");
	 if(auth2.getClient().getPrenom().equals(client1.getPrenom()))
	 fail("pas de changement prenom");
	 if(auth2.getClient().getTelephone().equals(client1.getTelephone()))
	 fail("pas de changement telephone");
	 if(auth2.getClient().getVersion() == versionAvant)
	 fail("pas de changement version");
	 
	
	 }
}
