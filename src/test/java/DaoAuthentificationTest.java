import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.*;
import org.junit.internal.runners.statements.Fail;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.fail;

import DAO.DaoAuthentification;
import DAO.DaoClient;
import Model.Authentification;
import Model.Client;

public class DaoAuthentificationTest  {

	@Test
	public void selectByIdMdpTest() {
		DaoAuthentification daoAut = new DaoAuthentification();
		Authentification auth = new Authentification();
		//DaoClient daoClient = new DaoClient();
		Client nouveauclt = new Client();
		String nom = "testnom";
		String prenom = "testprenom";
		String adresse = "testadresse";
		String telephone = "0011223344";
		nouveauclt.setNom(nom);
		nouveauclt.setPrenom(prenom);
		nouveauclt.setAdresse(adresse);
		nouveauclt.setTelephone(telephone);
		String login = "testselect";
		String mdp = "mdptestselect";
		auth.setLogin(login);
		auth.setMdp(mdp);
		daoAut.insertClient(auth, nouveauclt);
		auth = daoAut.selectByIdMdp(login, mdp);
		int idclient = auth.getClient().getId();
		// On supprime le client et Authentification cree pour pouvoir refaire le test a chaque fois
			//	daoClient.Delete(idclient);
				daoAut.Delete(login);

		assertTrue(auth.getLogin().equals(login));
		
		
	}

	@Test
	public void insertClientTest()  {
		// On ecrit cr�e un nouveau client
		DaoAuthentification daoAut=new DaoAuthentification();
		Authentification auth = new Authentification();
		String login = "testInsertClient";
		String mdp = "testInsertClient";
		auth.setLogin(login);
		auth.setMdp(mdp);
		auth.setDroit_acces(false);
		Client client = new Client();
		//client.setId(18);
		client.setNom("testinsertclientnom");
		client.setPrenom("testinsertclientprenom");
		client.setAdresse("testinsertclientnomadresse");
		daoAut.insertClient(auth, client);
		// On vient lire les informations du nouveau client, et on v�rifie si elles correspondent � ce que l'on � ins�r�

		DaoAuthentification daVerif =new DaoAuthentification();
		Authentification authVerif = new Authentification();
		authVerif = daVerif.selectByIdMdp(login, mdp);
		daoAut.Delete(login);
		if(!authVerif.getLogin().equals(login))
			fail("Login non reconnu");
		if(!authVerif.getMdp().equals(mdp))
			fail("Login non reconnu");
	}

}
