import static org.junit.Assert.fail;

import org.junit.Test;

import DAO.DaoProduit;
import Model.Fournisseur;
import Model.Produit;

public class DaoproduitTest {

	@Test
	public void updateTest() {
		// Ici on teste l'update en creant un produit, l'envoyer sur la BDD, cr�er un second produit avec le m�me id et le remplacer
		Fournisseur fourn = new Fournisseur();
		fourn.setId(1);
		fourn.setVersion(0);
		Produit prod1 = new Produit();
		prod1.setFournisseur(fourn);
		prod1.setId(11);
		prod1.setNom_produit("boum");
		prod1.setPrix(8);
		prod1.setStock(2);
		prod1.setType_produit("legumes");

		// On envoie le produit en BDD
		DaoProduit daop = new DaoProduit();
		daop.updateOrInsert(prod1);

		// On cr�e un second produit avec le m�me id

		Produit prod2 = new Produit();
		prod2.setFournisseur(fourn);
		prod2.setId(11);
		prod2.setNom_produit("zeze");
		prod2.setPrix(6);
		prod2.setStock(8);
		prod2.setType_produit("dessert");
		daop.updateOrInsert(prod2);

		if(prod1.getNom_produit()==prod2.getNom_produit())
			fail("nom de produit non update");
		if(prod1.getPrix()==prod2.getPrix())
			fail("Prix non update");
		if(prod1.getStock()==prod2.getStock())
			fail("Stock non update");
		if(prod1.getType_produit()==prod2.getType_produit())
			fail("type prod non update");
	}

}
