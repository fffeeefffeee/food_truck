import static org.junit.Assert.*;

import org.junit.Test;

import Model.CSVFile;

public class CSVFileTest {

	@Test
	public void lireBonDeCommandeTest() throws Exception {
		String testTrue = "testboncommandetrue.csv";
		String testFalseMoins = "testboncommandefalsemoins.csv";
		String testFalsePlus = "testboncommandefalseplus.csv";
		CSVFile csvF = new CSVFile();
		if(csvF.verifBonCommande(testFalsePlus))
			fail("ne detecte pas trop d'element dans le bon de commande");
		if(csvF.verifBonCommande(testFalseMoins))
			fail("ne detecte pas suffisamment d'element dans le bon de commande");
		if(!csvF.verifBonCommande(testTrue))
			fail("ne detecte une erreur sur un fichier de commande ouicorrect");
		
	}
}
