package User;

import Model.*;
import DAO.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class Menus {


    // Menu general qui permet de s'identifier et rediriger vers un autre menu
    public static void menuAuthentification() {
        // Initialisation du dao , utilisateur et scanner
        DaoAuthentification daoAuthentification = new DaoAuthentification();
        Authentification utilisateur = null;
        Scanner sclog = new Scanner(System.in);
        // Entrée des identifiants via console
        System.out.println("************************************");
        System.out.println("*** Bienvenue au Food Truck SKFN ***");
        System.out.println("************************************");
        System.out.print("Veuillez entrer votre login : ");
        String login = sclog.nextLine();
        System.out.print("Veuillez entrer votre mot de passe : ");
        String pass = sclog.nextLine();
        // On essaye de recuperer dans la BDD l'utilisateur correspondant
        try {
            utilisateur = daoAuthentification.selectByIdMdp(login, pass);
        } catch (Exception e) {
        }
        // Soit le client n'est en BDD.
        if (utilisateur == null) {
            System.out.println("Aucune correspondance trouvee entre login et mot de passe ");
            System.out.println("Voulez vous :");
            System.out.println("1. Reessayer ");
            System.out.println("2. Creer un compte ");
            String choice = sclog.nextLine();
            if (choice.equals("2")) {
                menuCreaCompte();
            }
        }
        // Soit on lance le menu client soit admin
        else {
            if (utilisateur.isDroit_acces())
                menuAdmin(utilisateur);
            else if (utilisateur.isDroit_acces() == false)
                menuClient(utilisateur);
        }
    }

    //menu création compte
    public static void menuCreaCompte() {
        DaoClient daoClient = new DaoClient();
        DaoAuthentification daoAuthentification = new DaoAuthentification();
        Scanner scCrea = new Scanner(System.in);
        String choice = "";
        Authentification utilisateur = new Authentification();
        Client client = new Client();
        utilisateur.setClient(client);
        client.setAuthentification(utilisateur);
        utilisateur.setDroit_acces(false);
        System.out.println("************************************");
        System.out.println("Saissisez Login :");
        choice = scCrea.nextLine();
        boolean loginExist = true;
        try {
            daoAuthentification.selectById(choice);
        } catch (Exception e) {
            loginExist = false;
        }
        if (loginExist) {
            System.out.println("Le login est deja pris");
        } else {
            utilisateur.setLogin(choice);
            System.out.println("Saissisez mdp :");
            choice = scCrea.nextLine();
            utilisateur.setMdp(choice);
            System.out.println("Saissisez nom :");
            choice = scCrea.nextLine();
            client.setNom(choice);
            System.out.println("Saissisez prenom :");
            choice = scCrea.nextLine();
            client.setPrenom(choice);
            System.out.println("Saissisez adresse :");
            choice = scCrea.nextLine();
            client.setAdresse(choice);
            System.out.println("Saissisez telephone :");
            choice = scCrea.nextLine();
            client.setTelephone(choice);
            daoAuthentification.insertClient(utilisateur, client);
            System.out.println("Client creer");
            System.out.println(utilisateur);
        }

    }

    //Menu Admin pour importer cvs et creer forunisseur
    public static void menuAdmin(Authentification utilisateur) {
        Scanner scAdmin = new Scanner(System.in);
        String infoAdmin = "";
        int nbInfoAdmin = 0;
        boolean continuer = true;
        while (continuer) {
            System.out.println("************************************");
            System.out.println("Voulez vous :");
            System.out.println("1_ Charger Bon de commande");
            System.out.println("2_ Entrer un fournisseur");
            System.out.println("3_ Verifier le stock de produits");
            System.out.println("99_ Quitter menu Admin");
            infoAdmin = scAdmin.nextLine();
            try {
                nbInfoAdmin = Integer.parseInt(infoAdmin);
            } catch (Exception e) {
                nbInfoAdmin = 0;
            }
            switch (nbInfoAdmin) {
                case 1:
                    boolean lu = true;
                    try {
                        lu = CSVFile.verifBonCommande("commande.csv");
                    } catch (Exception e) {
                        System.out.println("Fichier au mauvais format");
                        lu = false;
                    }
                    if (lu) {
                        try {
                            CSVFile.lireBonDeCommande("commande.csv");
                        } catch (Exception e) {
                            System.out.println("Erreur lecture fichier");
                            lu = false;
                        }
                    }
                    if (lu)
                        System.out.println("Fichier charge");
                    break;

                case 2:
                    menuFournisseur();
                    break;
                case 3:
                    affichageStocks();
                    break;
                case 99:
                    continuer = false;
                    break;
                default:
                    System.out.println("Erreur de saisie");
            }
        }
    }

    // Menu pour remplir les infos fournisseurs
    public static void menuFournisseur() {
        DaoFournisseur daoFournisseur = new DaoFournisseur();
        Scanner scFournisseur = new Scanner(System.in);
        String infoFournisseur = "";
        int nbInfoFournisseur = 0;
        boolean erreurSaisie = false;
        boolean idUtilise = true;
        Fournisseur fournisseur = new Fournisseur();
        System.out.println("************************************");
        System.out.println("Saissisez id");
        infoFournisseur = scFournisseur.nextLine();
        try {
            nbInfoFournisseur = Integer.parseInt(infoFournisseur);
        } catch (Exception e) {
            System.out.println("Ceci n'est pas un entier");
            erreurSaisie = true;
        }
        if (daoFournisseur.findById(nbInfoFournisseur) == null)
            idUtilise = false;
        if (idUtilise)
            System.out.println("id deja utilise");
        if (erreurSaisie == false && idUtilise == false) {
            fournisseur.setId(nbInfoFournisseur);
            System.out.println("Saissisez nom societe");
            infoFournisseur = scFournisseur.nextLine();
            fournisseur.setNom_societe(infoFournisseur);
            System.out.println("Saissisez adresse");
            infoFournisseur = scFournisseur.nextLine();
            fournisseur.setAdresse(infoFournisseur);
            System.out.println("Saissisez email");
            infoFournisseur = scFournisseur.nextLine();
            fournisseur.setEmail(infoFournisseur);
            daoFournisseur.insert(fournisseur);
        }
    }

    // Menu client
    public static void menuClient(Authentification utilisateur) {
        Commande commande = new Commande();
        // TODO générer un bon id quand on sauvegardera la commande
        commande.setNumeroCommande(1);
        commande.setAuthentification(utilisateur);
        Formule formule = null;
        Scanner scFormule = new Scanner(System.in);
        boolean continuer = true;
        while (continuer) {
            System.out.println("===========================");
            System.out.println("Etat commande");
            affichageCommande(commande);
            formule = new Formule();
            System.out.println("************************************");
            System.out.println("Chosissez une formule :");
            System.out.println("1_ Petit dejeuner ( Boisson Chaude + Dessert )");
            System.out.println("2_ Dejeuner ( Entree + Plat + Dessert + Boisson )");
            System.out.println("3_ Gouter ( Dessert + Boisson )");
            System.out.println("4_ Diner ( Plat + Dessert + Boisson )");
            System.out.println("99_ Annuler Commande");
            System.out.println("100_ Valider Commande");
            String choixFormule = scFormule.nextLine();
            int numFormule = 0;
            try {
                numFormule = Integer.parseInt(choixFormule);
            } catch (Exception e) {
                System.out.println("Ce n'est pas un entier");
            }
            // TODO fichier de configuration pour différente type de formules
            switch (numFormule) {
                case 1:
                    formule.setNomFormule("Petit dejeuner");
                    formule.getListeProduit().add(menuBoissonChaude());
                    formule.getListeProduit().add(menuDessert());
                    if (produitNonNullDansFormule(formule))
                        commande.getListeFormule().add(formule);

                    break;
                case 2:
                    formule.setNomFormule("Dejeuner");
                    formule.getListeProduit().add(menuEntree());
                    formule.getListeProduit().add(menuPlat());
                    formule.getListeProduit().add(menuDessert());
                    formule.getListeProduit().add(menuBoisson());
                    if (produitNonNullDansFormule(formule))
                        commande.getListeFormule().add(formule);
                    break;
                case 3:
                    formule.setNomFormule("Gouter");
                    formule.getListeProduit().add(menuDessert());
                    formule.getListeProduit().add(menuBoisson());
                    if (produitNonNullDansFormule(formule))
                        commande.getListeFormule().add(formule);
                    break;
                case 4:
                    formule.setNomFormule("Diner");
                    formule.getListeProduit().add(menuPlat());
                    formule.getListeProduit().add(menuDessert());
                    formule.getListeProduit().add(menuBoisson());
                    if (produitNonNullDansFormule(formule))
                        commande.getListeFormule().add(formule);
                    break;
                case 99:
                    System.out.println("Commande annulee");
                    //TODO remise en stock SI ANNULE
                    continuer = false;
                    break;
                case 100:
                    System.out.println("Commande validee");
                    menuLivraison(commande);
                    double prixNonLivre = affichageCommande(commande);
                    if(commande.isEstLivre())
                        System.out.println("La commande sera livre au "+commande.getAuthentification().getClient().getAdresse());
                    double prixLivre = (double) Math.round((prixNonLivre *1.10 )* 100) / 100;
                    System.out.println("Prix livre : "+ prixLivre);
                    System.out.println("===========================");
                    continuer = false;
                    break;
                default:
                    System.out.println("Erreur de saisie");
                    break;
            }
        }
    }

    public static void menuLivraison(Commande commande) {
        Scanner sclivre = new Scanner(System.in);
        String sLivre = "";
        int choixLivre = 0;
        boolean continuer = true;
        while (continuer) {
            System.out.println("Voulez etes livre ?");
            System.out.println("1_ Oui ?");
            System.out.println("2_ Non ?");
            sLivre = sclivre.nextLine();
            try {
                choixLivre = Integer.parseInt(sLivre);
            } catch (Exception e) {
            }
            switch (choixLivre) {
                case 1:
                    commande.setEstLivre(true);
                    continuer = false;
                    break;
                case 2:
                    commande.setEstLivre(false);
                    continuer = false;
                    break;
                default:
                    System.out.println("Erreur de saisie");
                    break;
            }
        }
    }

    public static Produit menuBoissonChaude() {
        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;
        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("boisson chaude");
        ArrayList<Produit> listaffichage = new ArrayList<>();

        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("************************************");
        System.out.println("Choissisez une boisson chaude :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.print("_ ");
            affichacheProduit(listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            affichacheProduit(produitcommande);
            daoProduit.updateOrInsert(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }

    public static Produit menuBoisson() {
        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;

        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("boisson");
        ArrayList<Produit> listaffichage = new ArrayList<>();
        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("************************************");
        System.out.println("Choissisez une boisson :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.print("_ ");
            affichacheProduit(listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            affichacheProduit(produitcommande);
            daoProduit.updateOrInsert(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }

    public static Produit menuEntree() {

        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;

        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("entree");
        ArrayList<Produit> listaffichage = new ArrayList<>();
        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("************************************");
        System.out.println("Choissisez une Entree :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.print("_ ");
            affichacheProduit(listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            affichacheProduit(produitcommande);
            daoProduit.updateOrInsert(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }

    public static Produit menuPlat() {
        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;
        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("plat");
        ArrayList<Produit> listaffichage = new ArrayList<>();
        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("************************************");
        System.out.println("Choissisez un Plat :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.print("_ ");
            affichacheProduit(listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            affichacheProduit(produitcommande);
            daoProduit.updateOrInsert(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }


    public static Produit menuDessert() {
        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;
        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("dessert");
        ArrayList<Produit> listaffichage = new ArrayList<>();
        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("************************************");
        System.out.println("Choissisez un Dessert :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.print("_ ");
            affichacheProduit(listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            affichacheProduit(produitcommande);
            daoProduit.updateOrInsert(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }

    public static double affichageCommande(Commande commande) {
        System.out.println("===========================");
        System.out.println("Commande n° " + commande.getNumeroCommande());
        System.out.println("De " + commande.getAuthentification().getClient().getNom() + " " + commande.getAuthentification().getClient().getPrenom());
        double total = 0.0;
        for (Formule form :
                commande.getListeFormule()) {
            total += affichageFormule(form);

        }

        double resultat = (double) Math.round(total * 100) / 100;
        System.out.println("Prix total : " + resultat);
        System.out.println("===========================");
        return resultat;
    }

    public static double affichageFormule(Formule formule) {
        double totalFormule = 0.0;
        System.out.println("Formule " + formule.getNomFormule());
        for (Produit prod :
                formule.getListeProduit()) {
            totalFormule += affichacheProduit(prod);
        }
        return totalFormule;
    }

    public static double affichacheProduit(Produit produit) {
        System.out.print("Nom : " + produit.getNom_produit() + ", ");
        System.out.println("Prix : " + produit.getPrix() + " ");
        return produit.getPrix();
    }

    public static void affichacheStockProduit(Produit produit) {
        System.out.print("Nom : " + produit.getNom_produit() + ", ");
        System.out.print("Stock : " + produit.getStock() + ", ");
        System.out.println("Prix : " + produit.getPrix() + " ");
    }

    public static void affichageStocks() {
        DaoProduit daoProduit = new DaoProduit();
        List<Produit> listeStock = new ArrayList<>();
        try {
            listeStock = daoProduit.findAll();
        } catch (Exception e) {
            System.out.println("Erreur de stock");
        }
        for (Produit p :
                listeStock) {
            affichacheStockProduit(p);

        }

    }

    public static boolean produitNonNullDansFormule(Formule formule) {
        boolean resultat = true;
        for (Produit p :
                formule.getListeProduit()) {
            if (p == null) {
                resultat = false;
            }
        }
        if (resultat == false) {
            System.out.println("Produit nul trouve dans la formule");
            System.out.println("Formule non ajoutee à la commande");
        }
        return resultat;
    }

    public static void main(String[] args) {
        Logger.getLogger("org.hibernate").setLevel(Level.OFF);
        while (true)
            menuAuthentification();


    }
}
