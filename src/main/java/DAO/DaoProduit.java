package DAO;

import java.util.List;

import javax.persistence.*;

import Model.Fournisseur;
import Model.Produit;

public class DaoProduit {

    // Supprime un produit en BDD via son id
    public void Delete(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Produit p = em.find(Produit.class, id);
        if (p != null)
            em.remove(p);
        tx.commit();
        em.close();
        emf.close();
    }


    // Recupere une liste de produits par sont type de produit
    public List<Produit> findByTypeProduit(String typeProduit) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("Select produit from Produit produit where produit.type_produit='" + typeProduit + "'");
        List<Produit> list = query.getResultList();
        em.close();
        emf.close();
        return list;
    }

    // Recupere tout les produits
    public List<Produit> findAll() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("Select produit from Produit produit");
        List<Produit> list = query.getResultList();
        em.close();
        emf.close();
        return list;
    }

    // Integre un produit en BDD , si il existe le met a jour
    public void updateOrInsert(Produit p) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Produit pverif = null;
        tx.begin();
        pverif = em.find(Produit.class, p.getId());


        //si produit non existant, on fait un persist pour qu'il soit manage
        if (pverif == null) {
            em.persist(p);
        }
        // S'il existe deja, il suffit de faire seulement un merge
        else {
            p.setVersion(pverif.getVersion());

            em.merge(p);
        }
        tx.commit();
        em.close();
        emf.close();
    }

    // Recupere un produit par son ID
    public Produit FindById(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        Produit p = em.find(Produit.class, id);
        em.close();
        emf.close();
        return p;
    }

    // Recuper  la quantite en stock d'un produit par son ID
    public int RecupStockById(int id) {
        int stock;
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        Produit prod = null;
        prod = em.find(Produit.class, id);
        if (prod == null)
            stock = 0;
        else
            stock = prod.getStock();

        em.close();
        emf.close();
        return stock;
    }
}
