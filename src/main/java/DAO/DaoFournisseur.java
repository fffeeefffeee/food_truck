package DAO;

import java.util.*;
import javax.persistence.*;

import Model.Fournisseur;

public class DaoFournisseur {

    // Efface en BBD la ligne de l'id
    public void Delete(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Fournisseur f = em.find(Fournisseur.class, id);
        if (f != null)
            em.remove(f);
        tx.commit();
        em.close();
        emf.close();
    }

    // Creer un nouveau fournisseur en BDD
    public void insert(Fournisseur f) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(f);
        tx.commit();
        em.close();
        emf.close();

    }

    // Recuper l'ensemble des fournisseur en BDD
    public List<Fournisseur> findAll() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("Select id from Fournisseur");
        List<Fournisseur> list = query.getResultList();
        em.close();
        emf.close();
        return list;
    }

    // Modifie un fournisseur en BDD
    public void update(Fournisseur f) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.merge(f);
        tx.commit();
        em.close();
        emf.close();
    }

    // Recupere un Fournisseur en fonction de son id
    public Fournisseur findById(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Fournisseur f = em.find(Fournisseur.class, id);
        em.close();
        emf.close();
        return f;
    }


}
