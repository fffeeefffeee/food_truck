package DAO;


import java.util.List;
import javax.persistence.*;
import Model.Authentification;
import Model.Client;

public class DaoAuthentification {

    // Permet de verifier la presence de  du login et mot de passe sur une même ligne  retourne null si pas present
    public Authentification selectByIdMdp(String login, String mdp) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select authentification  from  Authentification authentification where login='"
                + login + "' and mdp='" + mdp + "'");
        List<Authentification> list = query.getResultList();
        em.close();
        emf.close();
        if (list.get(0) != null)
            return list.get(0);
        else {
            // list.set(0, null);
            return null;
        }

    }

    // Selection par login d'un objet Authentification
    public String selectBylogin(String login) {
        Authentification auth = null;
        String log = "";
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        auth = em.find(Authentification.class, login);
        em.close();
        emf.close();
        if (auth != null)
            log = auth.getLogin();

        return log;
    }

    // Permet de verifier si le login est present en BDD
    public Authentification selectById(String login) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery(
                "select authentification  from  Authentification authentification where login='" + login + "'");
        List<Authentification> list = query.getResultList();
        em.close();
        emf.close();
        return list.get(0);

    }

    // Creer en une nouvelle entree en BDD d'un Authentification et Client liees
    public void insertClient(Authentification authentification, Client client) {

        authentification.setClient(client);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        em.persist(client);
        em.persist(authentification);

        tx.commit();
        em.close();
        emf.close();

    }

    // Efface un Auhentification en BDD
    public void Delete(String login) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Authentification auth = em.find(Authentification.class, login);
        if (auth != null)
            em.remove(auth);
        tx.commit();

        em.close();
        emf.close();
    }
}
