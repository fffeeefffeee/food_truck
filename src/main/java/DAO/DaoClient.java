package DAO;

import java.sql.*;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import Model.Authentification;
import Model.Client;
import Model.Produit;

public class DaoClient {

    // Donne le nombre de client en BDD
    public int nombreClient() {
        int resultat;
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select client  from  Client client");
        Collection<Client> list = query.getResultList();
        resultat = list.size();
        em.close();
        emf.close();
        return resultat;
    }

    // Modifie un Client existant
    public void update(Client client) throws ClassNotFoundException, SQLException {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        client = em.merge(client);
        tx.commit();
        em.close();
        emf.close();
    }

    // Inutilise pour le moment
    public Client selectByNomPrenom(String nom, String prenom) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        // pverif = em.find(Client.class, p.getId());
        Query query = em.createQuery("select authentification  from  Authentification authentification where nom='"
                + nom + "'and prenom ='" + prenom + "'");
        List<Client> list = query.getResultList();
        em.close();
        emf.close();
        return list.get(0);
    }

    // Efface en BBD la ligne de l'id
    public void Delete(int id) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Client client = em.find(Client.class, id);
        if (client != null)
            em.remove(client);
        tx.commit();
        em.close();
        emf.close();
    }
}
