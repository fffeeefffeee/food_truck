package Model;

import java.io.BufferedReader;
import java.io.FileReader;

import DAO.DaoProduit;

public class CSVFile {

	// Le nom du fichier de bon de commande est : commande.csv
	public static void lireBonDeCommande(String nomBonCommande ) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("src/main/"+nomBonCommande));

		String ligne = null;
		Produit prod = new Produit();
		Fournisseur fourn = new Fournisseur();
		DaoProduit daoProd = new DaoProduit();
		prod.setFournisseur(fourn);
		// Dans le while, on recupere ligne par ligne du fichier csv
		while ((ligne = br.readLine()) != null) {
			// Retourner la ligne dans un tableau
			// System.out.println("je lis une ligne");
			String[] data = ligne.split(";");
			prod.setId(Integer.parseInt(data[0]));
			prod.setNom_produit(data[1]);
			prod.setPrix(Double.parseDouble(data[2]) * 4);
			prod.getFournisseur().setId(Integer.parseInt(data[4]));
			prod.setType_produit(data[5]);

			// On recupere le stock actuel et on l'additionne au stock recu
			// du CSV
			int stockAvantAjout = daoProd.RecupStockById(prod.getId());
			prod.setStock(Integer.parseInt(data[3]) + stockAvantAjout);

			daoProd.updateOrInsert(prod);


		}
		br.close();
	}



	// Verifie l'intégrite du bon de commande
	public static boolean verifBonCommande(String nomBonCommande) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("src/main/"+nomBonCommande));
		String ligne = null;
		// Dans le while, on recupere ligne par ligne du fichier csv, on ve
		// verifier si le nombre d'element
		while ((ligne = br.readLine()) != null) {
			String[] data = ligne.split(";");
			if (data.length != 6){
				br.close();
				return false;
			}
		}
		br.close();
		return true;
	}
}
