package Model;

import javax.persistence.*;

@Entity
public class Produit {
    private int id;
    private String type_produit;
    private String nom_produit;
    private double prix;
    private int stock;
    private int version;
    private Fournisseur fournisseur;


    public Produit() {

    }


    @Id
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getType_produit() {
        return type_produit;
    }

    public void setType_produit(String type_produit) {
        this.type_produit = type_produit;
    }

    public String getNom_produit() {
        return nom_produit;
    }


    public void setNom_produit(String nom_produit) {
        this.nom_produit = nom_produit;
    }


    public double getPrix() {
        return prix;
    }


    public void setPrix(double prix) {
        this.prix = prix;
    }


    public int getStock() {
        return stock;
    }


    public void setStock(int stock) {
        this.stock = stock;
    }

    @ManyToOne
    @JoinColumn(name = "fournisseur_id")
    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    @Version
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }


    @Override
    public String toString() {
        return "Produit [id=" + id + ", type_produit=" + type_produit + ", nom_produit=" + nom_produit + ", prix="
                + prix + ", stock=" + stock + "]";
    }


}
