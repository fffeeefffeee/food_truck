package Model;

import java.util.*;

import javax.persistence.*;

@Entity
public class Fournisseur {
	private int id;
	private String nom_societe;
	private String adresse;
	private String email;
	private int version;
	private Collection<Produit> liste;
	
	
	public Fournisseur() {
		
	}
	
	@Id
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom_societe() {
		return nom_societe;
	}

	public void setNom_societe(String nom_societe) {
		this.nom_societe = nom_societe;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	 @Version
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	@OneToMany (mappedBy = "fournisseur")
	public Collection<Produit> getListe() {
		return liste;
	}

	public void setListe(Collection<Produit> liste) {
		this.liste = liste;
	}


	@Override
	public String toString() {
		return "Fournisseur{" +
				"id=" + id +
				", nom_societe='" + nom_societe + '\'' +
				", adresse='" + adresse + '\'' +
				", email='" + email + '\'' +
				", version=" + version +
				'}';
	}
}

