package Model;

import javax.persistence.*;

@Entity
public class Authentification {
    private String login;
    private String mdp;
    private boolean droit_acces;
    private int version;

    private Client client;


    @OneToOne
    @JoinColumn(name = "CLIENT_ID")
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Version
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }


    @Id
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public boolean isDroit_acces() {
        return droit_acces;
    }

    public void setDroit_acces(boolean droit_acces) {
        this.droit_acces = droit_acces;
    }

    public Authentification() {
        super();
    }

    public Authentification(String login, String mdp, boolean droit_acces) {
        super();
        this.login = login;
        this.mdp = mdp;
        this.droit_acces = droit_acces;
    }

    @Override
    public String toString() {
        return "Authentification{" +
                "login='" + login + '\'' +
                ", mdp='" + mdp + '\'' +
                ", droit_acces=" + droit_acces +
                ", version=" + version +
                ", client=" + client +
                '}';
    }
}
